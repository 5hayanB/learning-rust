# Learning-Rust

My Rust learning journey.

## Website
1. [rust-lang.org][1]
2. [The Rust Programming Language][2]
3. [The Cargo Book][3]

[1]: https://www.rust-lang.org/
[2]: https://doc.rust-lang.org/book/
[3]: https://doc.rust-lang.org/cargo/index.html

