fn main() {
    let mut s = "apple".to_string();
    println!("String: {s:?}");
    let vowels = "aeiou".to_string();
    let is_vowel = vowels.contains(&s[0..1]);
    if is_vowel {
        s.push_str("-hay");
    } else {
        let consonant = s.remove(0);
        s.push_str(&format!("-{consonant}ay"));
    }
    println!("Pig Latin: {s:?}");
}
