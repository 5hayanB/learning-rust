#[derive(Debug)]
struct Rectangle {
    width: u32,
    heigth: u32
}

impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.heigth
    }
    fn width(&self) -> bool {
        self.width > 0
    }
    fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.heigth > other.heigth
    }
    fn square(size: u32) -> Self {
        Self {
            width: size,
            heigth: size
        }
    }
}

fn area(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.heigth
}

fn main() {
    let scale = 2;
    let rect1 = Rectangle {
        width: dbg!(30 * scale),
        heigth: 50
    };
    let rect2 = Rectangle {
        width: 30,
        heigth: 50
    };
    let rect3 = Rectangle {
        width: 10,
        heigth: 40
    };
    let rect4 = Rectangle {
        width: 60,
        heigth: 45
    };
    let sq = Rectangle::square(3);
    println!("The area of the rectangle is {} square pixels.", area(&rect1));
    println!("rect1 is {rect1:#?}");
    dbg!(&rect1);
    println!("The area of the rectangle is {} square pixels.", rect2.area());
    if rect2.width() {
        println!("The rectangle has a nonzero width; it is {}", rect2.width);
    }
    println!("Can rect2 hold rect3? {}", rect2.can_hold(&rect3));
    println!("Can rect2 hold rect4? {}", rect2.can_hold(&rect4));
    dbg!(&sq);
}
