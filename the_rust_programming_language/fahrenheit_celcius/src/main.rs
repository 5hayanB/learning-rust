fn celsius_to_fahrenheit(c: f32) -> f32 {
    (c * 1.8) + 32.0
}


fn fahrenheit_to_celsius(f: f32) -> f32 {
    (f - 32.0) * (5.0 / 9.0)
}


fn main() {
    let celsius = 25.0;
    println!("Temperature in Celsius: {celsius}");
    println!("Converted Fahrenheit temperature: {}", celsius_to_fahrenheit(celsius));
    println!("");
    let fahrenheit = 80.0;
    println!("Temperature in Fahrenheit: {fahrenheit}");
    println!("Converted Celsius temperature: {}", fahrenheit_to_celsius(fahrenheit));
}
