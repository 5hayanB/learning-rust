use std::collections::HashMap;


fn median(v: &mut Vec<i32>) -> i32 {
    if !v.is_sorted() {
        v.sort();
    }
    match v.len() & 1 {
        0 => v[(v.len() / 2) - 1],
        1 => v[v.len() / 2],
        _ => 0
    }
}

fn mode(v: Vec<i32>) -> i32 {
    let mut mode_map = HashMap::new();
    for e in &v {
        let count = mode_map.entry(e).or_insert(0);
        *count += 1;
    }
    let mut largest_count_key = mode_map.get(&v[0]).copied().unwrap_or(0);
    for (key, val) in &mode_map {
        if mode_map.get(&largest_count_key).copied().unwrap_or(0) < *val {
            largest_count_key = **key;
        }
    }
    largest_count_key
}

fn main() {
    let mut v = vec![
        5, 6, 2, 3,
        5, 8, 5, 3,
        5, 6, 4, 5,
        4, 4, 8, 4,
        5, 5, 6, 5,
        4, 6, 9, 8,
        6, 4, 4, 3,
        4, 3, 4, 5,
        4, 5, 4, 5,
        4, 5, 5, 3,
        3, 7, 4, 2
    ];
    println!("Median = {}", median(&mut v));
    println!("Mode = {}", mode(v));
}
