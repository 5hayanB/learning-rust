use std::{
    collections::HashMap,
    io::{self, Write}
};

fn add_employee(employees: &mut HashMap<String, Vec<String>>) {
    let mut department = String::new();
    let mut emp_name = String::new();
    println!("\nADD EMPLOYEE\n");
    print!("Department: ");
    io::stdout().flush().expect("flushed");
    io::stdin().read_line(&mut department).expect("Failed to read line");
    print!("Employee Name: ");
    io::stdout().flush().expect("flushed");
    io::stdin().read_line(&mut emp_name).expect("Failed to read line");
    department = department.trim().to_string();
    emp_name = emp_name.trim().to_string();
    match employees.get_mut(&department) {
        Some(emp_vec) => {
            emp_vec.push(emp_name);
            if let Some(last_emp) = emp_vec.last() {
                println!("{last_emp} added to {department}");
            }
        },
        None => {
            println!("{emp_name} added to {department}");
            employees.insert(department, vec![emp_name]);
        }
    };
}

fn list_employees(employees: &mut HashMap<String, Vec<String>>) {
    let mut department = String::new();
    println!("\nLIST EMPLOYEES\n");
    print!("Department: ");
    io::stdout().flush().expect("flushed");
    io::stdin().read_line(&mut department).expect("Failed to read line");
    department = department.trim().to_string();
    let emp_list = match employees.get_mut(&department) {
        Some(emp_vec) => emp_vec,
        None => &mut Vec::new()
    };
    if !emp_list.is_empty() {
        emp_list.sort();
        for emp_name in emp_list {
            println!("{emp_name}");
        }
    } else {
        println!("Invalid department");
    }
}

fn main() {
    let mut employees = HashMap::new();
    loop {
        println!("\nWhat would you like to do?");
        println!("[1] Add an employee to a department");
        println!("[2] List all people in a department");
        println!("[0] Quit");
        print!("Enter action number: ");
        let mut action = String::new();
        io::stdout().flush().expect("flushed");
        io::stdin().read_line(&mut action).expect("Failed to read line");
        let action: u8 = match action.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Enter an action number");
                continue;
            }
        };
        match action {
            0 => break,
            1 => add_employee(&mut employees),
            2 => list_employees(&mut employees),
            _ => println!("Invalid action number. Please try again\n")
        }
    }
}
